<?php
    extract($_POST);
    $category = explode(',', $category);

    require 'connect.php';
    $sql='select id, product from tbl_products';
    $query = mysqli_query($con, $sql);

    foreach ( $query as $row ) :
        
        $row_id = $row['id'];
        $row_product = $row['product'];

        if ( $row_id == $id )
            die("SKU já cadastado no produto: $row_product");

    endforeach;


    if ($product == '') :
        echo 'Defina um nome para o produto';

    elseif ($price == '') :
        echo 'Defina um preço para o produto';

    elseif ($amount == '') :
        echo 'Defina uma quantidade para o produto';

    elseif ( !isset($category) ) :
        echo 'Escolha uma categoria para o produto';

    else :
    
        $filename = $_FILES['img']['name'];
        $extension = pathinfo($filename,PATHINFO_EXTENSION);

        $img = "$id.$extension";
        $target = "../assets/images/product/$img";

        $valid_extensions = array("jpg","jpeg","png","gif");

        if( !in_array(strtolower($extension),$valid_extensions) )
            die("Insira uma imagem válida");
        

        if ( move_uploaded_file($_FILES['img']['tmp_name'],$target) ) :
            
            $sql = "insert into tbl_products(id, product, price, amount, description, img) values($id, '$product', '$price', '$amount', '$description', '$img')";
            $query = mysqli_query($con, $sql)or die($sql);

            foreach ($category as $cat) {
                $sql = "insert into tbl_product_category(product_id, category_id) values('$id', '$cat')";
                $query = mysqli_query($con, $sql)or die('Erro ao cadastrar categoria');
            }

            echo 'ok';

        else:

            echo "Erro ao fazer upload da imagem.";

        endif;
        
    endif;
