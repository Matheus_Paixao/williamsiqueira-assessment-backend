<?php 
		require 'inc/connect.php';

		$con = mysqli_connect($host, $user, $pass, $base);
		$sql="select * from tbl_categories";
		$query = mysqli_query($con, $sql);
?>


  <main class="content">
    
    <div class="header-list-page">
      <h1 class="title">Categorias</h1>
      <a href="adicionar-categoria" class="btn-action">Adicionar</a>
    </div>

    <table class="data-grid">

      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categoria</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Código</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Opções</span>
        </th>
      </tr>
      
      <?php foreach( $query as $row ): ?>
      <?php extract($row); ?>
      
        <tr class='data-row'>

          <td class='data-grid-td'>
            <?= $category; ?>
          </td>
        
          <td class='data-grid-td' id="id">
            <?= $id; ?>
          </td>
        
          <td class='data-grid-td'>
            <a class="edit" href="editar-categoria/<?= $id; ?>">Editar</a>
            <a class="delete" onclick="delete_category(<?= $id; ?>)">Deletar</a>
          </td>
        
        </tr>

      <?php endforeach; ?>
    </table>
  </main> 


  <script>
    delete_category = (id) => {
      data = `id=${id}&table=tbl_categories`;

      request = new XMLHttpRequest;
      request.open('POST', 'inc/delete.php', true);
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      request.send(data);

      request.onload = () => {
        response = request.responseText;
        response == 'ok' ? window.location = 'categorias' :  alert(response);
      }

    }
  </script>