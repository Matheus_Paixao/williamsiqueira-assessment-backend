<?php 
		require 'inc/connect.php';
		$sql="select * from tbl_products";
    $query = mysqli_query($con, $sql);
?>


  <main class="content">
    
    <div class="header-list-page">
      <h1 class="title">Produtos</h1>
      <a href="adicionar-produto" class="btn-action">Adicionar Produto</a>
    </div>

    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Produto</span>
        </th>

        <th class="data-grid-th">
        <span class="data-grid-cell-content">Foto</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Preço</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantidade</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categoria</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Opções</span>
        </th>
      </tr>

      <?php foreach( $query as $row ): ?>
      <?php extract($row);?>

        <tr class="data-row">

          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= $product; ?></span>
          </td>

          <td>
            <?php if($img == ''): ?>
              <img src="https://www.freeiconspng.com/uploads/no-image-icon-4.png" height="50"> 
            <?php else: ?>
              <img src="assets/images/product/<?= $img; ?>"  height="50"> 
            <?php endif; ?>
          </td>
        
          <td class="data-grid-td">
            <span id="id" class="data-grid-cell-content"><?= $id; ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content">R$ <?= number_format($price, 2, ',', '.'); ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= $amount; ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content">
            <?php 
              $sql="select category from tbl_categories
                    inner join tbl_product_category
                    on tbl_categories.id = tbl_product_category.category_id
                    WHERE tbl_product_category.product_id = $id";
              
              $query = mysqli_query($con, $sql);
       
              foreach( $query as $row ){
                extract($row);
                echo $category .'<br>';
              }
            ?>

            
            </span>
          </td>
        
          <td class='data-grid-td'>
            <a class="edit" href="editar-produto/<?= $id; ?>">Editar</a>
            <br>
            <a class="delete" onclick="delete_product(<?= $id; ?>)">Deletar</a>
          </td>
        </tr>
      <?php endforeach; ?>
      
    </table>
  </main>


<script>
  delete_product = (id) => {
    data = `id=${id}&table=tbl_products`;

    request = new XMLHttpRequest;
    request.open('POST', 'inc/delete.php', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.send(data);

    request.onload = () => {
      response = request.responseText
      response == 'ok' ? window.location = 'produtos' :  alert(response);
    }

  }
</script>