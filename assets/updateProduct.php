<?php 
    require 'inc/connect.php';
    $sql="select * from tbl_products where id = $id";
    $query = mysqli_query($con, $sql);

    foreach( $query as $row ){
        extract($row);
    }
?>

  <main class="content">
    <h1 class="title new-item">Atualizar Produto</h1>
    
    <form>
      <input type="hidden" id="old_id" value="<?= $id; ?>" /> 
      <input type="hidden" id="id" class="input-text" value="<?= $id; ?>" /> 

      <div class="input-field">
        <label for="product" class="label">Produto</label>
        <input type="text" id="product" class="input-text" value="<?= $product; ?>"  /> 
      </div>

      <div class="input-field">
        <label class="label">Foto</label>
        <img class="thumb" src="assets/images/product/<?= $img; ?>" width="150">
      </div>

      <div class="input-field"> 
      <label for="img" class="label">&nbsp;</label>
        <div style="display: inline-block; ;line-height: 49px; width: 500px">
          <form>
            <input type="file" id="img"/>
            <input type="button" class="button" value="Upload" id="btn-upload">
          </form>
        </div>
      </div>

      <div class="input-field">
        <label for="price" class="label">Preço</label>
        <input type="number" id="price" class="input-text" value="<?= $price; ?>"  /> 
      </div>
      
      <div class="input-field">
        <label for="amount" class="label">Quantidade</label>
        <input type="number" id="amount" class="input-text" value="<?= $amount; ?>"  /> 
      </div>
      
      <div class="input-field">
        <label for="category" class="label">Categorias</label>

        <select multiple id="category" class="input-text" value="<?= $category; ?>" >
            <?php 
                $sql="select * from tbl_categories";
                $query = mysqli_query($con, $sql);
                foreach( $query as $row ) :
                    $row_id = $row['id'];
                    $row_category = $row['category'];
                    echo "<option value='$row_id'>$row_category</option>";
                endforeach;
            ?>
        </select>
      </div>

      <div class="input-field">
        <label for="description" class="label">Descrição</label>
        <textarea id="description" class="input-text"><?= $description; ?></textarea>
      </div>

      <div class="actions-form">
        <a href="produtos" class="action back">Voltar</a>
        <input class="btn-submit btn-action" type="button" value="Salvar" />
      </div>
      
    </form>
  </main>


<script>
  $('.btn-submit').on('click', ()=> {
    id          = $('#id').val();
    old_id      = $('#old_id').val();
    price       = $('#price').val();
    amount      = $('#amount').val();
    product     = $('#product').val();
    category    = $('#category').val();
    description = $('#description').val();

    $.ajax({
      url: './inc/update.php',
      type: 'post', 
      data: {
        'id': id,
        'old_id': old_id,
        'price': price,
        'amount': amount,
        'product': product,
        'category': category,
        'description': description,
        'action': 'updateProduct'
      },

      success: (response) => {
        response == 'ok' ? window.location = 'produtos' : alert(response);
      }

    });
  });

  $("#btn-upload").on('click', () => {
    fd = new FormData();
    
    id = $('#id').val();
    img = $('#img')[0].files[0];

    fd.append('id', id);
    fd.append('img', img);

    $.ajax({
        url: './inc/upload.php',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: (response) => {
            response == 'ok' ?  location.reload() : alert(response);
        }
      });
    });

</script>
