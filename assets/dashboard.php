<?php 
		require 'inc/connect.php';
		$sql="select * from tbl_products";
    $query = mysqli_query($con, $sql);
?>

<main class="content">
  
  <div class="header-list-page">
    <h1 class="title">Dashboard</h1>
  </div>
  
  <div class="infor">
    <a href="adicionar-produto" class="btn-action">Adicionar Produto</a>
  </div>

  <ul class="product-list">

    <?php foreach( $query as $row ): ?>
    <?php extract($row);?>

    <li>
  
      <div class="product-image">

          <?php if($img == ''): ?>
            
            <img src="https://www.freeiconspng.com/uploads/no-image-icon-4.png" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />

          <?php else: ?>

            <img src="assets/images/product/<?= $img; ?>" layout="responsive" width="164" height="145" alt="<?= $product; ?>" />

          <?php endif; ?>

      </div>

      <div class="product-info">
        
        <div class="product-name"><?= $product; ?></div>
      
        <div class="product-price">
          <span class="special-price"><?= $amount > 1 ? "$amount Disponíveis" : "$amount Disponível"; ?> </span>
          <br> <span>R$ <?= number_format($price, 2, ',', '.'); ?></span>
        </div>

      </div>

    </li>
    <?php endforeach; ?>
    
  </ul>

</main>
