  <main class="content">
    
    <h1 class="title new-item">Nova Categoria</h1>
    
    <form onsubmit="addCategory(this); return false;">
      
      <div class="input-field">
        <label for="category" class="label">Categoria</label>
        <input type="text" id="category" class="input-text" />
      </div>

      <div class="actions-form">
        <a href="categoria" class="action back">Voltar</a>
        <input class="btn-submit btn-action"  type="button" value="Salvar" onclick="add_category(this)"/>
      </div>

    </form>

  </main>

  <script>

    add_category = function(element) {

      action   = 'insertCategory';
      submit   = document.querySelector('.btn-submit');
      category = document.querySelector('#category').value;
          
      data = `action=${action}&category=${category}`;

      submit.setAttribute('disabled', 'true');
      submit.innerHTML = 'Enviando...';

      request = new XMLHttpRequest;
      request.open('POST', 'inc/insert-category.php', true);
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      request.send(data);

      request.onload = () => {
        response = request.responseText;
        response == 'ok' ? window.location = 'categorias' :  alert(response);
      }

    }
  </script>