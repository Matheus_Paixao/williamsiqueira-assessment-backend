<?php 
		require 'inc/connect.php';
		$sql="select * from tbl_categories";
    $query = mysqli_query($con, $sql);
?>

  <main class="content">
    
    <h1 class="title new-item">Novo Produto</h1>
    
    <form action="inc/insert-product.php" method="post" enctype="multipart/form-data">

      <div class="input-field">
        <label for="id" class="label">SKU</label>
        <input type="number" id="id" class="input-text" /> 
      </div>

      <div class="input-field">
        <label for="product" class="label">Produto</label>
        <input type="text" id="product" class="input-text" /> 
      </div>

      <div class="input-field">
        <label for="img" class="label">Foto</label>
        <input type="file" id="img" class="input-text" /> 
      </div>

      <div class="input-field">
        <label for="price" class="label">Preço</label>
        <input type="number" id="price" class="input-text" /> 
      </div>
      
      <div class="input-field">
        <label for="amount" class="label">Quantidade</label>
        <input type="number" id="amount" class="input-text" /> 
      </div>
      
      <div class="input-field">
        <label for="category" class="label">Categorias</label>

        <select multiple id="category" class="input-text">
          <?php foreach( $query as $row ): ?>
          <?php extract($row); ?>
            <option value="<?= $id; ?>"><?= $category; ?></option>
          <?php endforeach; ?>
        </select>
      </div>

      <div class="input-field">
        <label for="description" class="label">Descrição</label>
        <textarea id="description" class="input-text"></textarea>
      </div>

      <div class="actions-form">
        <a href="/produtos" class="action back">Voltar</a>
        <input class="btn-submit btn-action" type="button" value="Salvar" />
      </div>
      
    </form>
  </main>


<script>

  $('.btn-submit').on('click', () => {
    
    fd = new FormData();

    id          = $('#id').val();
    img         = $('#img')[0].files[0];
    price       = $('#price').val();
    amount      = $('#amount').val();
    product     = $('#product').val();
    category    = $('#category').val();
    description = $('#description').val();

    fd.append('id', id);
    fd.append('img', img);
    fd.append('price', price);
    fd.append('amount', amount);
    fd.append('product', product);
    fd.append('category', category);
    fd.append('description', description);
      
    $.ajax({
      url: './inc/insert-product.php',
      type: 'post', 
      data: fd,
      contentType: false,
      processData: false,
      
      success: (response) => {
        if (response == 'ok') {
          window.location = 'produtos';
        }else{
          alert(response)
        }
      }

    })
  });
</script>
